package com.automation;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.apache.commons.io.FileUtils;

public class GPS extends ChromeBaseAutomationTest {

	@Test
	public void inputTextTest() throws Exception {
		// url de la p�gina
		driver.get("http://spe001001-336/oim/Formulario/login.aspx");
		// capturar pantallas
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("c://Evidencias//imagen1.png"));

		// LOGIN PAGE
		driver.findElement(By.id("ddlRol")).sendKeys("EJECUTIVO MAPFRE");

		// Ingresar usuario
		driver.findElement(By.id("txtUsuario")).sendKeys("WEBMASTER");

		// Ingresar contrase�a
		driver.findElement(By.id("txtClave")).sendKeys("123");

		// capturar pantallas

		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("c://Evidencias//imagen2.png"));

		// Seleccionar boton ingresar
		driver.findElement(By.id("btnIngresar")).click();

		// Seleccionar el link de la aplicaci�n
		// driver.findElement(By.id("gdvAplicacion_ctl88_imgOpcion")).click();

		// capturar pantallas

		File scrFile2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile2, new File("c://Evidencias//imagen3.png"));

		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript("fn_IngresarAplicacion('GPS')");

			// capturar pantallas

			File scrFile3 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile3, new File("c://Evidencias//imagen4.png"));

			WebElement linkChito = driver.findElement(By.xpath("//a[text() = 'CARGA EXCEL']"));

			// Ejecutar aunque no sea visible cuando no funcione el linkChito.click()
			// directamente.
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkChito);

			// capturar pantallas

			File scrFile4 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile4, new File("c://Evidencias//imagen5.png"));

			Thread.sleep(1000);
			// Seleccionar boton ingresar
			driver.findElement(By.id("ContentPlaceHolder1_btnBuscar")).click();
			
			Thread.sleep(10000);
			System.out.println("Hola");
		}
	}
}
