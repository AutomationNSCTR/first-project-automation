package com.automation;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class IEBaseAutomationTest {
	
	protected WebDriver driver;
	
//	@SuppressWarnings("deprecation")
	@Before
	public void crearDriver() {
		String currentDir = System.getProperty("user.dir");		
		String driverLocation = currentDir + "/drivers/IEDriverServer.exe";		
		System.setProperty("webdriver.ie.driver", driverLocation);
//		DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
//		caps.setCapability("ignoreZoomSetting", true);
//		driver = new InternetExplorerDriver(caps);
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		driver = new InternetExplorerDriver(capabilities);
		driver.manage().window().maximize();
		
	//@After
//	public void cerrarDriver() {
		//driver.quit();
		
	}
	}
	

