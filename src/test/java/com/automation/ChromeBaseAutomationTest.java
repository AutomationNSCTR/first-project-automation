package com.automation;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class ChromeBaseAutomationTest {
	
	protected WebDriver driver;
	
	@Before
	public void crearDriver() {
		String currentDir = System.getProperty("user.dir");		
		String driverLocation = currentDir + "/drivers/chromedriver.exe";		
		System.setProperty("webdriver.chrome.driver", driverLocation);
		driver = new ChromeDriver();
	}

	@After
	public void cerrarDriver() {
		driver.quit();
	}

}
